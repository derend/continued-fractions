#lang scribble/manual
@(require scribble/eval
          (for-label racket
                     "main.rkt" "create.rkt"))

@(define this-eval (make-base-eval))
@title[#:tag "cf-create-top"]{Custom Continued Fractions}
@defmodule[continued-fractions/create]
@section{Creation Concepts}
@subsection{Creation Introduction}
There are two standard types of continued fractions. @italic{Simple} continued fractions
are of the form
@nonbreaking{a0 + 1/(a1 + 1/(a2 + ...))}. @italic{General} continued fractions are of the form
@nonbreaking{d0 + n0/(d1 + n1/(d2 + ...))}. This library offers some procedures, sequence utilities, and sequences
to help you make your own continued fractions.
@subsection{Creation Caveats}
As mentioned in the main @seclink["Introduction" #:doc '(lib "continued-fractions/cf-manual.scrbl")]{introduction},
continued fractions in this library generally only allow the first non-zero term to be negative. Short
of enumerating all the terms of a sequence, there is no way to guarantee this. Thus, @bold{hand-crafted
 continued fractions for exploratory purposes may violate some assumptions of this library.}

This library operates under the additional assumption that all generated terms of continued fractions are exact
integers. One may always transform an arbitrary continued fraction with rational terms to one with
integer terms. For instance, in the continued fraction @nonbreaking{1 + (3/2)/(1 + ...)} the denominator
of 2 can be eliminated by multiplying this portion of the continued fraction by 2/2, giving
@nonbreaking{1 + 3/(2 + (2* ...)).}

@section{Provided Sequences}
The forms provided by
@seclink["more-sequences" #:doc '(lib "scribblings/reference/reference.scrbl")]{@racket[racket/sequence]} are
recommended but not provided. In particular, many continued fractions have initial terms that do not follow a pattern,
while subsequent terms do, so @racket[sequence-append] is helpful. Additionally, some specific sequences are given.
@subsection{Sequence Utilities}
@defproc[(sequence-map* [p procedure?] [s sequence?] ...) sequence?]{Like @racket[map], but for sequences. If any sequence dies,
 then this sequence dies, since it can no longer be guaranteed to succeed in applying the procedure argument.}
@interaction-eval[#:eval this-eval
                  (require "main.rkt" "create.rkt")]
@examples[#:eval this-eval
          (require racket/sequence)
          (let ((S (sequence-map* + (in-naturals 0) (in-naturals 0) (in-range 5))))
            (sequence->list S))]
@defproc[(every-other [s sequence?]) sequence?]{Omits every other term.}
@examples[#:eval this-eval
          (sequence->list (every-other (in-range 10)))]
@defproc[(interleave [s sequence?] ...) sequence?]{Takes a term from each sequence in argument order before continuing any sequence.
 If any sequence dies it is culled from the interleaving action and the main sequence continues until there are no inner sequences left
 producing terms.}
@examples[#:eval this-eval
          (sequence->list
           (interleave (in-range 0 10) (in-range 10 15) (in-range 100 103)))]
@subsection{Specific Sequences}
@defproc[(endless-values [v any/c]) sequence?]{Endless values of the argument.}
@defproc[(in-evens [v (and/c exact-integer? even?)]) sequence?]
@defproc[(in-odds [v (and/c exact-integer? odd?)]) sequence?]{Sequences of even and odd numbers, respectively.}
@examples[#:eval this-eval
          (for/list ((t (in-evens 10))
                     (i (in-range 10)))
            t)
          (for/list ((t (in-odds 9))
                     (i (in-range 10)))
            t)]
@defproc[(in-squares [v number?]) sequence?]{An endless sequence of squares, starting with the first square that is
 greater than or equal to the argument.}
@examples[#:eval this-eval
          (for/list ((s (in-squares 9))
                     (i (in-range 10)))
            s)
          (for/list ((s (in-squares 8))
                     (i (in-range 10)))
            s)]
@defproc[(in-cubes [v number?]) sequence?]{Like @racket[sqaures].}
@examples[#:eval this-eval
          (for/list ((c (in-cubes 0))
                     (i (in-range 10)))
            c)]
@defproc[(in-triangle [v number?]) sequence?]{A sequence of triangle numbers. Triangle numbers are of the form
 @nonbreaking{(n*(n+1))/2.}}
@defproc[(in-double-triangle [v number?]) sequence?]{A sequence of triangle numbers multiplied by 2.}
@examples[#:eval this-eval
          (for/list ((t (in-triangle 0))
                     (i (in-range 10)))
            t)
          (equal? (for/list ((t (in-triangle 0))
                             (i (in-range 10)))
                    (* 2 t))
                  (for/list ((t (in-double-triangle 0))
                             (i (in-range 10)))
                    t))]
@defproc[(in-common-difference [init (and/c exact? integer?)]
                               [base-sequence sequence?]
                               [#:strip-until strip (or/c #f (and/c exact? integer?)) #f])
         sequence?]{The first term is init. The second term is init plus the first term of the
 base-sequence. The third term is the second term plus the second term of the
 base-sequence. And so on. The optional argument, if present, will automatically go through
 the sequence until the term is greater than or equal to strip.}
@examples[#:eval this-eval
          (sequence->list (in-common-difference 0 (in-range 10)))
          (for/list ((t (in-common-difference
                         0 (in-common-difference
                            1 (in-common-difference
                               6 (endless-values 6)))))
                     (i (in-range 10)))
            t)]

@section{Continued Fraction Creation}
@subsection{Continued Fraction Procedures}
Two main forms are provided for continued fractions.
@defproc[(sequence->simple-continued-fraction [s sequence?]
                                              [#:force count (or/c #f
                                                                   (and/c exact? positive? integer?))
                                               #f])
         continued-fraction?]{Creates the continued fraction @nonbreaking{a0 + 1/(a1 + 1/(a2 + ...))} from the
 given sequence. For the keyword argument, see @seclink["cf-forcing"]{Forcing}.}
@defproc[(sequences->general-continued-fraction [d sequence?]
                                                [n sequence?]
                                                [#:force count (or/c #f
                                                                     (and/c exact? positive? integer?))
                                                 #f])
         continued-fraction?]{Creates the continued fraction @nonbreaking{d0 + n0/(d1 + n1/(d2 + ...))} from
 the @italic{d} and @italic{n} sequences. For the keyword argument, see @seclink["cf-forcing"]{Forcing}.}
@subsection[#:tag "cf-forcing"]{Forcing}
Well-behaved continued fractions have terms which are always positive or always negative. Unfortunately this
is not always the case. Sometimes there is a crossover where some terms switch from being positive to being
negative. A sequence provided may at some point emit a zero at a known location.
Such terms can cause havoc with continued fraction arithmetic. For that reason, the @racket[#:force]
keyword is provided. It allows the user to internally force the consumption of the specified number of
terms before any arithmetic or other output is attempted. This overrides any @racket[precision] or
@racket[consume-limit] parameters.

As an example, the continued fraction @nonbreaking{(-3 -2 -1 0 1 2 3 ...)} is equivalant to
@nonbreaking{(-3 -2 0 2 3 ...)} which is eventually just @nonbreaking{(0 4 5 6 ...)}. Since
internal algorithms assume that no zeros will appear, emission of a term from a
continued fraction sequence may occur prematurely if @racket[#:force] is not used.

Such emission does not affect the accuracy of continued fraction arithmetic, but it does invalidate
guarantees of @racket[precision-emit] or the correctness of terms given by @racket[base-emit].
@examples[#:eval this-eval
          (define bad-cf
            (sequence->simple-continued-fraction (in-range -5 15)))
          (sequence->list bad-cf)
          (define good-cf
            (sequence->simple-continued-fraction (in-range -5 15) #:force 11))
          (sequence->list good-cf)
          @code:comment{The overall accuracy of arithmetic is not affected:}
          @code:comment{these are both valid representations of the same rational.}
          (= (cf-terms->rational (sequence->list bad-cf))
             (cf-terms->rational (sequence->list good-cf)))
          (for/list ((t (base-emit bad-cf 10))
                     (i (in-range 20)))
            t)
          (for/list ((t (base-emit good-cf 10))
                     (i (in-range 20)))
            t)]
